package com.example

import org.slf4j.LoggerFactory
import ch.qos.logback.classic.Logger

object Hello {

  val logger = LoggerFactory.getLogger(Hello.getClass).asInstanceOf[Logger]

  def main(args: Array[String]): Unit = {
    println("Hello, world!")

    x()

  }


  def x() = {
    logger.info("LOGGING")
  }
}
