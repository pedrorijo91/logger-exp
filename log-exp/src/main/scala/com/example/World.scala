package com.example

import ch.qos.logback.classic.Logger
import org.slf4j.LoggerFactory

/**
 * Created by pedrorijo on 02/08/15.
 */
object World {

  val logger = LoggerFactory.getLogger(World.getClass).asInstanceOf[Logger]

  def main(args: Array[String]): Unit = {
    println("Hello, world!")

    x()

  }


  def x() = {
    logger.info("LOGGING")
  }
}